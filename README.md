# WEB Backend Test

Petstore By Ganjar Setia

[Demo Petstore](http://petstore.aaganjar.com/)

# Requirement

 - git
 - PHP >= 5.6.4
 - composer

# Instalasi

Clone repo dengan command:

    git clone https://ganjar_setia@bitbucket.org/ganjar_setia/ganjar_backend_test.git

    composer install
    cp .env.example .env 
    php artisan key:generate

Buat database
Edit file .env. Ubah koneksi database dibawah ini.
    
    DB_HOST=127.0.0.1
    DB_DATABASE=homestead
    DB_USERNAME=homestead
    DB_PASSWORD=secret

lalu jalankan command

    php artisan migrate
    php artisan db:seed
    chmod -R 775 storage
    chmod -R 775 bootstrap/cache

jalankan:

    php artisan serve

buka [http://localhost:8000/](http://localhost:8000/)

# Login
Login sebagai admin:
    
    user: admin@aaganjar.com
    pass: admin

# List API Route
Listnya bisa di lihat di [http://localhost:8000/routes](http://localhost:8000/routes) atau
dicoba online di  [http://localhost:8000/api/docs](http://localhost:8000/api/docs)
Sample API get all pet [http://localhost:8000/api/v1/pet/](http://localhost:8000/api/v1/pet/)

### Notes
untuk API `/api/v1/pets/{id}` method PUT, parameter `photo` harap menggunakan encode base64.

# Created By: Ganjar Setia