<?php

namespace App\Http\Controllers;

use App\DataTables\PetDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePetRequest;
use App\Http\Requests\UpdatePetRequest;
use App\Repositories\PetRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class PetController extends AppBaseController
{
    /** @var  PetRepository */
    private $petRepository;

    public function __construct(PetRepository $petRepo)
    {
        $this->petRepository = $petRepo;
    }

    /**
     * Display a listing of the Pet.
     *
     * @param PetDataTable $petDataTable
     * @return Response
     */
    public function index(PetDataTable $petDataTable)
    {
        return $petDataTable->render('pets.index');
    }

    /**
     * Show the form for creating a new Pet.
     *
     * @return Response
     */
    public function create()
    {
        return view('pets.create');
    }

    /**
     * Store a newly created Pet in storage.
     *
     * @param CreatePetRequest $request
     *
     * @return Response
     */
    public function store(CreatePetRequest $request)
    {
        $input = $request->all();

        // Set filename for photo
        if ($request->file('photo')) {
            $imageName = time() . '.' . $request->file('photo')->getClientOriginalExtension();

            $input['photo'] = $imageName;
        }

        // save all input
        $pet = $this->petRepository->create($input);

        // move photo file
        if ($pet && $request->file('photo')) {
            $request->file('photo')->move(base_path() . '/public/images/', $imageName);
        }

        Flash::success('Pet saved successfully.');

        return redirect(route('pets.index'));

    }

    /**
     * Display the specified Pet.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pet = $this->petRepository->findWithoutFail($id);

        if (empty($pet)) {
            Flash::error('Pet not found');

            return redirect(route('pets.index'));
        }

        return view('pets.show')->with('pet', $pet);
    }

    /**
     * Show the form for editing the specified Pet.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pet = $this->petRepository->findWithoutFail($id);

        if (empty($pet)) {
            Flash::error('Pet not found');

            return redirect(route('pets.index'));
        }

        return view('pets.edit')->with('pet', $pet);
    }

    /**
     * Update the specified Pet in storage.
     *
     * @param  int              $id
     * @param UpdatePetRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePetRequest $request)
    {
        $pet = $this->petRepository->findWithoutFail($id);

        if (empty($pet)) {
            Flash::error('Pet not found');

            return redirect(route('pets.index'));
        }

        $input = $request->all();

        // Set filename for photo
        if ($request->file('photo')) {
            $imageName = time() . '.' . $request->file('photo')->getClientOriginalExtension();

            $input['photo'] = $imageName;
        }

        $pet = $this->petRepository->update($input, $id);

        // move photo file
        if ($pet && $request->file('photo')) {
            $request->file('photo')->move(base_path() . '/public/images/', $imageName);
        }

        Flash::success('Pet updated successfully.');

        return redirect(route('pets.index'));
    }

    /**
     * Remove the specified Pet from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pet = $this->petRepository->findWithoutFail($id);

        if (empty($pet)) {
            Flash::error('Pet not found');

            return redirect(route('pets.index'));
        }

        $this->petRepository->delete($id);

        Flash::success('Pet deleted successfully.');

        return redirect(route('pets.index'));
    }
}
