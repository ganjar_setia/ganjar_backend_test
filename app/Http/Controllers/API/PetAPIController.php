<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePetAPIRequest;
use App\Http\Requests\API\UpdatePetAPIRequest;
use App\Models\Pet;
use App\Repositories\PetRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PetController
 * @package App\Http\Controllers\API
 */

class PetAPIController extends AppBaseController
{
    /** @var  PetRepository */
    private $petRepository;

    public function __construct(PetRepository $petRepo)
    {
        $this->petRepository = $petRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/pets",
     *      summary="Get a listing of the Pets.",
     *      tags={"Pet"},
     *      description="Get all Pets",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Pet")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->petRepository->pushCriteria(new RequestCriteria($request));
        $this->petRepository->pushCriteria(new LimitOffsetCriteria($request));
        $pets = $this->petRepository->all();

        return $this->sendResponse($pets->toArray(), 'Pets retrieved successfully');
    }

    /**
     * @param CreatePetAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/pets",
     *      summary="Store a newly created Pet in storage",
     *      tags={"Pet"},
     *      description="Store Pet",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Pet that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Pet")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Pet"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePetAPIRequest $request)
    {
        $input = $request->all();

        // Set filename for photo
        if ($request->file('photo')) {
            $imageName = time() . '.' . $request->file('photo')->getClientOriginalExtension();

            $input['photo'] = $imageName;
        }

        $pets = $this->petRepository->create($input);

        // move photo file
        if ($pets && $request->file('photo')) {
            $request->file('photo')->move(base_path() . '/public/images/', $imageName);
        }

        return $this->sendResponse($pets->toArray(), 'Pet saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/pets/{id}",
     *      summary="Display the specified Pet",
     *      tags={"Pet"},
     *      description="Get Pet",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Pet",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Pet"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Pet $pet */
        $pet = $this->petRepository->findWithoutFail($id);

        if (empty($pet)) {
            return $this->sendError('Pet not found');
        }

        return $this->sendResponse($pet->toArray(), 'Pet retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePetAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/pets/{id}",
     *      summary="Update the specified Pet in storage",
     *      tags={"Pet"},
     *      description="Update Pet",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Pet",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Pet that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Pet")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Pet"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update(Request $request, $id)
    {

        $input = $request->all();
        $image = '';

        /** @var Pet $pet */
        $pet = $this->petRepository->findWithoutFail($id);

        if (empty($pet)) {
            return $this->sendError('Pet not found');
        }

        // Set filename for photo
        if ($input['photo']) {
            $image = base64_decode($input['photo']);
            $imageName = time() . '.png';
            $input['photo'] = $imageName;
        }

        $pet = $this->petRepository->update($input, $id);

        // save photo file
        if ($pet && $image) {
            file_put_contents(base_path() . '/public/images/'.$imageName, $image);
        }

        return $this->sendResponse($pet->toArray(), 'Pet updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/pets/{id}",
     *      summary="Remove the specified Pet from storage",
     *      tags={"Pet"},
     *      description="Delete Pet",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Pet",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Pet $pet */
        $pet = $this->petRepository->findWithoutFail($id);

        if (empty($pet)) {
            return $this->sendError('Pet not found');
        }

        $pet->delete();

        return $this->sendResponse($id, 'Pet deleted successfully');
    }

    public function uploadImage($id, Request $request)
    {
        $input = $request->all();

        // Set filename for photo
        if ($request->file('photo')) {
            $imageName = time() . '.' . $request->file('photo')->getClientOriginalExtension();

            $input['photo'] = $imageName;
        }

        $pets = $this->petRepository->update(['photo'=>$input['photo']], $id);

        // move photo file
        if ($pets && $request->file('photo')) {
            $request->file('photo')->move(base_path() . '/public/images/', $imageName);
        }

        return $this->sendResponse($pets->toArray(), 'Pet photo update successfully');
    }
}
