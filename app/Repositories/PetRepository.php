<?php

namespace App\Repositories;

use App\Models\Pet;
use InfyOm\Generator\Common\BaseRepository;

class PetRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'age',
        'photo'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pet::class;
    }
}
